var express = require("express");
app = express();

var bodyParser = require("body-parser");
app.use(bodyParser.json());

var database = [];
var currentId = 0;

app.get("/users", function (request, response) {
  response.json(database);
});

app.post("/users", function (request, response) {
  let user = request.body;
  user.id = currentId++;
  database.push(user);
  response.end();
});

app.get("/users/:id", function (request, response) {
  let user = database.find(x => x.id == request.params.id);
  response.json(user || null);
});

app.listen(9090);
