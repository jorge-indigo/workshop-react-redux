import React from 'react';

class Formulario extends React.Component {

  constructor() {
    super();

    this.state = {
      nombre: "Jorge",
      apellido: ""
    }
  }

  actualizarNombre(texto) {
    console.log("Estoy actualizando el nombre");
    this.setState({ nombre: texto });
  }

  actualizarApellido(texto) {
    this.setState({ apellido: texto });
  }

  render() {
    return (
      <div className="container">
        <h1>Formulario</h1>
        <h2>Nombre en el estado: { this.state.nombre }</h2>
        <h2>Apellido en el estado: { this.state.apellido }</h2>
        <form>
          <div className="form-group">
            <label>Nombre:</label>
            <input type="text"
                className="form-control"
                onChange={ e => this.actualizarNombre(e.target.value) }
                value={ this.state.nombre }/>
          </div>
          <div className="form-group">
            <label>Apellido:</label>
            <input type="text"
                className="form-control"
                onChange={ e => this.actualizarApellido(e.target.value) }
                value={ this.state.apellido }/>
          </div>

        </form>
      </div>
    );
  }
}

export default Formulario;
