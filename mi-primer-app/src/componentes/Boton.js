import React from 'react';

class Boton extends React.Component {

  constructor(props) {
    super(props);

    this.veces = 0;
    this.state = {
      veces: 0,
      textoBoton: "Clickeame"
    };
  }

  render() {

    return (
      <div>
        <p>El saludo inicial es: { this.props.saludines }</p>
        <button onClick={ this.clickeado.bind(this) }>{ this.state.textoBoton }</button>
        <p>Veces que has hecho click: <strong>{ this.state.veces }</strong></p>
      </div>
    );
  }

  clickeado() {
    this.veces++;
    this.setState({ veces: this.veces });
    this.props.modificarTitulo(this.veces);
  }
}

export default Boton;
