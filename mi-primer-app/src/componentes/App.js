import React from 'react';
import Texto from './Texto';
import BotonHTML from './Boton';

class App extends React.Component {

  constructor() {
    super();

    this.state = {
      saludo: "Hola Inmundo!"
    };
  }

  render() {
    return (
      <div>
        <h1>{ this.state.saludo }</h1>
        <Texto />
        <BotonHTML
            saludines={ this.state.saludo }
            modificarTitulo={ this.modificarTitulo.bind(this) } />
      </div>
    );
  }

  modificarTitulo(veces) {
    if (veces == 10) {
      this.setState({ saludo: "Feliciades llegaste a 10!" });
    }
  }
}

export default App;
