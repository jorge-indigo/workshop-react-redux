import React from 'react';

class Texto extends React.Component {

  constructor() {
    super();

    this.state = {
      texto: "Esto es un componente interno !!!!"
    }
  }

  render() {
    let texto = this.state.texto;
    texto = texto.toUpperCase();
    return <p>{ texto }</p>;
  }
}

export default Texto;
