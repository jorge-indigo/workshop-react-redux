import React from 'react';
import ReactDOM from 'react-dom';
import Formulario from './componentes/Formulario';

ReactDOM.render(<Formulario />, document.getElementById('root'));
