export default class Persona {

  constructor(nombre, apellido) {
    this.nombre = nombre;
    this.apellido = apellido;
  }

  comer() {
    console.log("yum yum yum");
  }
}
