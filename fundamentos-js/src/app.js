/*console.log("Hola Mundo");

setTimeout(function () {
  console.log("Hello World");
}, 3000);

console.log("Hola Inmundo");*/

/*var promesa = new Promise(function (resolve, reject) {
  console.log("Hola Mundo");
  resolve();
});

promesa
  .then(function () {
    console.log("La promesa anterior se resolvio de forma exitosa");
  })
  .then(function () {
    console.log("Hola Inmundo");
  })
  .then(function () {
    var promesa2 = new Promise(function (resolve, reject) {
      setTimeout(function () {
        console.log("Ya pasaron 5s");
        reject();
      }, 5000);
    });

    return promesa2;
  })
  .then(function () {
    console.log("Fin de la cadena");
  })
  .catch(function () {
    console.log("La cadena termino en error");
  });
  */

function ajax(milisegundos) {
  var promise = new Promise(function (resolve) {
    setTimeout(function () { resolve() }, milisegundos);
  });

  return promise;
}

// Callback
ajax(3000, function () {

  ajax(200, function() {

    ajax(1000, function () {
      console.log("La otra peticion ha terminado");

      ajax(500, function () {
        console.log("La 4ta peticion ha terminado");

        ajax(3000, function () {
          console.log("La ultima funcion ha terminado");
        });
      });
    });
  });
});


ajax(3000)
  .then(function () {
    console.log("1ra Peticion");
    return ajax(200);
  })
  .then(function () {
    console.log("2nd Peticion");
    return ajax(1000);
  })
  .then(function () {
    console.log("3ra Peticion");
  })


window.mostrarAviso = function () {
  alert("Me has pillado");
}
