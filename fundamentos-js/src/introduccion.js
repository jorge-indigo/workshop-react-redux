/* Variables */
/* Posibles valores
 * String ""
 * Boolean true, false
 * Number
 * Arreglos
 * Objetos
 * Funciones
 */
var algunNombre = "Hector";
var unBooleano = true;
var unNumero = 1000;
var unArreglo = [];
//var unObjeto = {};

/* Las variables creadas por var, solo viven dentro de la funcion mas cercana */
var unaFuncion = function () {
  var loQueSea = 0;
}

/* Las variables creadas por let, solo viven dentro de las llaves mas cercanas */
let otroNombre = "Luis";

/* Siguen las mismas reglas que let, no se pueden volver a asignar */
const unNombreMas = "Jorge";
//unNombreMas = "Ilse";

/* Las funciones son bloques de codigo, que hacen un accion por particular */
let f1 = function () {}

function f2() {}

let f3 = () => {
}

function suma(a, b) {
  return a + b;
}

//console.log(suma(3,6));


function aplicarF(f) {
  return f(0);
}

/*console.log(aplicarF(function (algo) {
  return algo + 1;
}));*/

// Closures
function closure() {
  var contador = 0;

  return function () {
    return ++contador;
  }
}

let utilizandoClosure = closure();

// Arreglos
let miArreglo = [];
let numeritos = [10,20,30,40,50];
let cosas = [1, true, "Hola", [], {}];

cosas.push("Este es un curso de React");
cosas.shift();

//console.log("la longitud es", cosas.length);
cosas.splice(1, 1);
//console.log("la longitud es", cosas.length, cosas);

// Recorrer el arreglo
// #1 Utilizando un for normal
console.log(numeritos);

let buscarEnNumeritos = (indice, elementoEncontrado, elementoBuscado) => {
  if (elementoEncontrado == elementoBuscado) {
    console.log("Encontraste el", elementoBuscado, "en la posicion", indice);
  }
}

for (let i = 0; i < numeritos.length; i++) {
  buscarEnNumeritos(i, numeritos[i], 40);
}

// #2 Utilizando el for por indices
for (let indice in numeritos) {
  buscarEnNumeritos(indice, numeritos[indice], 20);
}

// #3 Utilizando el for of
for (let elemento of numeritos) {
  buscarEnNumeritos("No lo se :(", elemento, 30);
}

// #4 Utilizando el método forEach
numeritos.forEach((elemento, indice) => buscarEnNumeritos(indice, elemento, 10));


// Los objetos
let unObjeto = {};
let unObjeto2 = { valor1: 0, valor2: true, funcion: function () {
  return "una funcion dentro de un objeto"
}};

console.log(unObjeto2.funcion());

let persona = {
  nombre: "Ilse",
  apellido: "Espetia",
  edad: 25,
  hablar: function () {
    console.log("Holiii !");
  },
  beber: function () {
    console.log("Bebiendo glu glu glu glu ...");
  },
  nombreCompleto: function () {
    setTimeout(() => {
      console.log(this.nombre + " " + this.apellido);
    }, 5000);
  }
}



//console.log(persona.hablar());
//persona.beber();
//console.log(persona.nombreCompleto());
//persona.queEsThis();

/* Clases */
// ES5
function Persona(nombre, apellido, edad) {
  this.nombre = nombre;
  this.apellido = apellido;
  this.edad = edad;
}

// Prototipos
Persona.prototype.hablar = function () {
  console.log("blah blah blah");
}

Persona.prototype.beber = function(bebida) {
  console.log(this.nombre, " esta bebiendo", bebida, " glu glu glu");
}

let p = new Persona("Natalia", "Ramon", 3);
let q = new Persona("Ariana", "Espinosa", 8);
p.hablar();
p.beber("Chocolate");

Persona.prototype.conducir = function () {
  console.log("rum rum");
}

p.conducir();
q.conducir();

String.prototype.toHelloWorld = function () {
  return "Hello World! " + this;
}

let ejemploPrototype = "Saul";
console.log(ejemploPrototype.toHelloWorld());


class Animal {

  constructor(nombre) {
    this.nombre = nombre;
  }

  muerete() {
    console.log(this.nombre, " estoy muerto ... x.x");
  }
}

class Canino extends Animal {

  constructor(nombre, raza) {
    super(nombre);
    this.raza = raza;
  }

  ladrar() {
    console.log("woufff");
  }
}

let a = new Animal("Gato");
a.muerete();

let c = new Canino("Dinky", "Westy");
c.ladrar();
c.muerete();

console.log(a instanceof Animal);
console.log(c instanceof Canino);
console.log([] instanceof Array);
