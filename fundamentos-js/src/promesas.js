var promise = new Promise(function (resolve, reject) {
  console.log("Hola 1");
  resolve();
})

promise
  .then(function () {
    console.log("Hola 2");
  })
  .then(function () {

    let promise = new Promise(function (resolve, reject) {
      setTimeout(function () {
        console.log("Hola 3");
        resolve();
      });
    });

    return promise;
  })
  .then(function () {
    console.log("Hola 4");
  });


/*
setTimeout(function () {
  console.log("Hola 2");
});

console.log("Hola 3");*/
